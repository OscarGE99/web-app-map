import { createRequire } from "module";
const require = createRequire(import.meta.url);
const nextPWA = require("next-pwa");

const withPWA = nextPWA({
  dest: "public",
});

/** @type {import('next').NextConfig} */
const nextConfig = {
  output: "export",
};

export default withPWA(nextConfig);
