interface ExtendableEvent extends Event {
  waitUntil(fn: Promise<any>): void;
}

interface FetchEvent extends Event {
  request: Request;
  respondWith(response: Promise<Response> | Response): void;
}

self.addEventListener("install", (event: Event) => {
  const evt = event as ExtendableEvent;
  console.log("Service Worker: Installed");
});

self.addEventListener("activate", (event: Event) => {
  const evt = event as ExtendableEvent;
  console.log("Service Worker: Activated");
});

self.addEventListener("fetch", (event: Event) => {
  const evt = event as FetchEvent;
  evt.respondWith(
    caches.match(evt.request).then((response) => {
      return response || fetch(evt.request);
    })
  );
});
