import { NextRequest, NextResponse } from "next/server";

export async function POST(request: NextRequest) {
  try {
    const { latitude, longitude } = await request.json();
    console.log("Received location:", latitude, longitude);
    // Aquí puedes procesar y almacenar la ubicación como desees
    return NextResponse.json({ message: "Location received" });
  } catch (error) {
    console.error("Error processing location:", error);
    return NextResponse.json({ message: "Invalid request" }, { status: 400 });
  }
}
