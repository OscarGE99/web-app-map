"use client";

import { useEffect, useState } from 'react';
import dynamic from 'next/dynamic';

// Cargar el componente del mapa dinámicamente para evitar problemas de SSR
const Map = dynamic(() => import('../components/Map'), { ssr: false });

export default function Home() {
  const [watchID, setWatchID] = useState<number | null>(null);
  const [coordinates, setCoordinates] = useState<{ latitude: number; longitude: number } | null>(null);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker
          .register('/sw.js')
          .then((registration) => {
            console.log('Service Worker registered with scope:', registration.scope);
          })
          .catch((error) => {
            console.log('Service Worker registration failed:', error);
          });
      });
    }

    return () => {
      if (watchID !== null) {
        navigator.geolocation.clearWatch(watchID);
      }
    };
  }, [watchID]);

  const startTracking = () => {
    if (navigator.geolocation) {
      const id = navigator.geolocation.watchPosition(sendPosition, handleError, {
        enableHighAccuracy: true,
        maximumAge: 0,
        timeout: 10000 // Aumentar el tiempo de espera a 10 segundos
      });
      setWatchID(id);
      setError(null); // Limpiar cualquier error previo
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  };

  const stopTracking = () => {
    if (watchID !== null) {
      navigator.geolocation.clearWatch(watchID);
      setWatchID(null);
    }
  };

  const sendPosition = (position: GeolocationPosition) => {
    const { latitude, longitude } = position.coords;
    setCoordinates({ latitude, longitude });

    fetch('/api/location', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ latitude, longitude })
    })
      .then(response => response.json())
      .then(data => console.log(data))
      .catch(error => console.error('Error:', error));
  };

  const handleError = (error: GeolocationPositionError) => {
    let errorMessage = '';
    switch (error.code) {
      case error.PERMISSION_DENIED:
        errorMessage = 'User denied the request for Geolocation.';
        break;
      case error.POSITION_UNAVAILABLE:
        errorMessage = 'Location information is unavailable.';
        break;
      case error.TIMEOUT:
        errorMessage = 'The request to get user location timed out.';
        break;
      default:
        errorMessage = 'An unknown error occurred.';
        break;
    }
    setError(errorMessage);
    console.warn(`ERROR(${error.code}): ${error.message}`);
  };

  return (
    <main className="flex min-h-screen flex-col items-center justify-center p-24">
      <h1>Welcome to the Home Page</h1>
      <button onClick={startTracking}>Start Tracking</button>
      <button onClick={stopTracking}>Stop Tracking</button>
      {coordinates && (
        <div>
          <p>Latitude: {coordinates.latitude}</p>
          <p>Longitude: {coordinates.longitude}</p>
          <Map latitude={coordinates.latitude} longitude={coordinates.longitude} />
        </div>
      )}
      {error && (
        <div style={{ color: 'red' }}>
          <p>{error}</p>
        </div>
      )}
    </main>
  );
}